function preenche(matriz) {
    var tamanhoi = prompt("Qual é o numero de linhas?")
    var tamanhoj = prompt("Qual é o numero de colunas?")
    for (var i = 0; i < tamanhoi; i++) {
        var linha = [];
        for (var j = 0; j < tamanhoj; j++) {
            var valor = prompt("Qual é o valor da linha " + i + " coluna " + j);
            linha.push(valor);
        }
        matriz.push(linha);
    }
}

function verifica(matriz1, matriz2) {
    if (matriz1.length != matriz2[0].length) {
        return 0;
    }
    return 1;
}

function multiplica(matriz1, matriz2, matriz3) {
    // preenche matriz
    for (var i = 0; i < matriz1.length; i++) {
        var linha = [];
        for (var j = 0; j < matriz1[0].length; j++) {
            linha.push(0);
        }
        matriz3.push(linha);
    }

    // muda valor da matriz
    for (var i = 0; i < matriz1.length; i++) {
        for (var j = 0; j < matriz1.length; j++) {
            for (var z = 0; z < matriz1.length; z++) {
                matriz3[i][j] += matriz1[i][z] * matriz2[z][j];
            }
        }
    }
}

var matriz1 = [];
var matriz2 = [];
var matriz3 = [];

preenche(matriz1);
console.log(matriz1);
preenche(matriz2);
console.log(matriz2);

if (verifica(matriz1, matriz2) == 1){
    multiplica(matriz1, matriz2, matriz3);
    console.log(matriz3); 
} else {
    console.log("Multiplicação inválida.\nTente novamente.");
}