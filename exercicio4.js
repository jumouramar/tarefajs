// objetos não tem propriedade .lenght, então eu criei
function qtd(elem){
    var tam = 0;
    for(n in elem){
        tam++;
    }
    return tam;
}

// Q1
gods.forEach((god) => { console.log(god.name + " " + qtd(god.features)) })

// Q2
gods.forEach((god) => { 
    for(var i = 0; i < qtd(god.roles); i++){
       if(god.roles[i] == "Mid"){
            console.log(god)
        }   
    }
})


// Q3
gods.sort(function (a, b) {
    if (a.pantheon < b.pantheon) {
      return -1;
    }
    if (a.pantheon > b.pantheon) {
      return 1;
    }
    return 0;
});
console.log(gods)

// Q4
var novoArray = gods.map((god) => {
    return god.name + " (" + god.class + ")"
})
console.log(novoArray)
